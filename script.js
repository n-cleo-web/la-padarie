const fila = document.getElementById('fila')
const pessoas = document.getElementById('pessoas')
const vendidos = document.getElementById('vendidos')
const valorEntrada = document.getElementById('valor-entrada')
const modal = document.getElementById('modal')
const botaoAdd = document.getElementById('btn-add') 
const botaoEnviar = document.getElementById('btn-enviar')
var botaoDelete = document.getElementsByClassName('delete')
const nome = document.getElementById('input-nome')
const paes = document.getElementById('input-pao')

function reload() {
    var pedidos = document.getElementsByClassName('pedidos')
    pessoas.innerHTML = pedidos.length

    var pao = document.getElementsByClassName('total-pao')
    var totalPao = 0
    for (let i = 0; i < pao.length; i++) {
        totalPao += parseInt(pao[i].innerHTML, 10) 
    }
    vendidos.innerHTML = totalPao

    var pagar = document.getElementsByClassName('total-pagar')
    var totalPagar = 0
    for (let i = 0; i < pagar.length; i++) {
        totalPagar += parseFloat(pagar[i].innerHTML)
    }
    valorEntrada.innerHTML = "R$ " + totalPagar.toFixed(2)

    botaoDelete = document.getElementsByClassName('delete')
    Array.from(botaoDelete).forEach(function(botaoDelete) {
        botaoDelete.addEventListener('click', removerPedido)
        botaoDelete.addEventListener('touchstart', removerPedido)
    })
}

function abrirModal(event) {
    if (event.type == 'touchstart') {
        event.preventDefault()
    }

    modal.classList.add('active')

    modal.addEventListener('click', (e) => {
        if (e.target == modal || e.target.id == 'btn-cancel') {
            modal.classList.remove('active')
            nome.value = ''
            paes.value = ''
        }
    })
    modal.addEventListener('touchstart', (e) => {
        if (e.target == modal || e.target.id == 'btn-cancel') {
            modal.classList.remove('active')
            nome.value = ''
            paes.value = ''
            event.preventDefault()
        }
    })
}

function addPedido(e) {

    if (e.type == 'touchstart') {
        e.preventDefault()
    }
    if (nome.value != '' || paes.value != '') {
        custo = paes.value/2

        fila.innerHTML +=
        `<div class="pedidos">
            <div>
                <h3>` + nome.value + `</h3>
                <div>
                    <p>Total de pães: <span class="total-pao">`+ paes.value + `</span><span> pães</span></p>
                    <p>Total a pagar: <span>R$ </span><span class="total-pagar">` + custo.toFixed(2) + `</span></p>
                </div>
            </div>
            <img src="./Images/Icon4.svg" alt="" class="delete">
        </div>`;
        
        modal.classList.remove('active')
        nome.value = ''
        paes.value = ''
        reload()
    }
}

function removerPedido(e) {
    if (e.type == 'touchstart') {
        e.preventDefault()
    }

    pai = e.target.parentElement
    pai.innerHTML = ''
    pai.remove()
    reload()
}

reload()

botaoAdd.addEventListener('click', abrirModal)
botaoAdd.addEventListener('touchstart', abrirModal)

botaoEnviar.addEventListener('click', addPedido)
botaoEnviar.addEventListener('touchstart', addPedido)